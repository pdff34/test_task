from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://www.ozon.ru/"

    def find_element(self, locator, time=10):
        return WebDriverWait(self.driver,time).until(EC.presence_of_element_located(locator),
                                                     message=f"Can't find element by locator {locator}")
    def find_elements(self, locator, time=10):
        return WebDriverWait(self.driver,time).until(EC.presence_of_all_elements_located(locator),
                                                     message=f"Can't find elements by locator {locator}")
    def element_is_visible(self, locator, time=10):
        return WebDriverWait(self.driver,time).until(EC.visibility_of_element_located(locator),
                                                     message=f"Element not visible by locator {locator}")
    def element_is_clickable(self, locator, time=10):
        return WebDriverWait(self.driver,time).until(EC.element_to_be_clickable(locator),
                                                     message=f"Element not clickable by locator {locator}")
    def go_to_site(self):
        return self.driver.get(self.base_url)

    def scroll_to_element(self, element, heigh=0):
        locator = self.find_element(element,time=20)
        for i in range (1, 20):
            self.driver.execute_script("arguments[0].scrollIntoView();", locator)
        self.driver.execute_script(f"scrollBy(0,{heigh})")
        self.element_is_visible(element,time=20)
        self.find_element(element,time=20)

    def swith_to_new_tab_by_href(self, href):
        self.driver.switch_to.new_window('tab')
        self.driver.get(href)





