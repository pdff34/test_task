from PageObject.BaseApp import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains


class MainPageLocators:

    SEARCH_FIELD_FOR_PRODUCT = (By.XPATH , "//input[@placeholder='Искать на Ozon']")
    SEARCH_BUTTON = (By.XPATH, "//button[@type='submit']")
    SEARCHING_RESULTS = (By.XPATH, "//div[contains(@class, 'widget-search-result-container')]")
    DELIVERY_ADDRESS = (By.XPATH, "//div[@data-widget='addressBookBarWeb']//button")
    CHANGE_ADDRESS = (By.XPATH, "//span[text() = 'Изменить']")
    PICK_CITY_FOR_TEST = (By.XPATH, "//div[text() = 'Санкт-Петербург']")
    VIEW_TEST_CITY = (By.XPATH, "//span[text() = 'Санкт-Петербург']")
    CATALOG_BUTTON = (By.XPATH, "//span[text()='Каталог']/ancestor::button")
    CATALOG_HOME_AND_GARDEN = (By.XPATH, "//span[text()='Дом и сад']")
    CATALOG_OPTION_MIRRORS = (By.XPATH, "//span[text()='Зеркала']")


class MainPage(BasePage):

    '''
    Вводим в поисковую строку искомый товар.
    '''
    def enter_word(self, word):
        search_field = self.find_element(MainPageLocators.SEARCH_FIELD_FOR_PRODUCT,time=20)
        search_field.click()
        search_field.send_keys(word)

    '''
    Нажатие на кнопку поиска.
    '''
    def click_on_the_search_button(self):
        self.find_element(MainPageLocators.SEARCH_BUTTON,time=20).click()

    '''
    Выбираем город для теста, в котором точно будет в наличии несколько rtx 3090.
    '''
    def pick_city_for_search_rtx(self):
        self.find_element(MainPageLocators.DELIVERY_ADDRESS,time=20).click()
        self.find_element(MainPageLocators.CHANGE_ADDRESS,time=20).click()
        self.find_element(MainPageLocators.PICK_CITY_FOR_TEST,time=20).click()
        self.find_element(MainPageLocators.VIEW_TEST_CITY,time=20)

    '''
    Открытие каталога.
    '''
    def go_to_catalog_page(self):
        self.find_element(MainPageLocators.CATALOG_BUTTON,time=20).click()

    '''
    Проверка наличия опции "Зеркала" в категории "Дом и сад".
    '''
    def select_catalog_and_check_home_and_garden(self):
        element_to_hover_over = self.find_element(MainPageLocators.CATALOG_HOME_AND_GARDEN,time=20)
        hover = ActionChains(self.driver).move_to_element(element_to_hover_over)
        hover.perform()
        self.find_element(MainPageLocators.CATALOG_OPTION_MIRRORS,time=20)

