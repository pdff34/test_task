from PageObject.BaseApp import BasePage
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import re

class ProductPageLocators:
    SECOND_PICTURES_IN_LIST = (By.XPATH, "//div[@data-index='1']//img")
    PRODUCT_PAGE_LOADED = (By.XPATH, "//a[@href='https://job.ozon.ru/']")


class ProductPage(BasePage):

    '''
    Дожидаемся загрузки страницы, выбираем вторую картинку товара и делаем скриншот.
    '''
    def select_second_pictures(self):
        second_pictures = self.find_element(ProductPageLocators.SECOND_PICTURES_IN_LIST,time=20)
        second_pictures_src = second_pictures.get_attribute('src')
        match = re.search('(\d+)(?!.*\d)', second_pictures_src).group(0)
        locator_for_picture = f"//img[contains(@src, '{match}') and @fetchpriority]"
        SELECTED_SECOND_PICTURES = (By.XPATH, locator_for_picture)

        self.find_element(ProductPageLocators.PRODUCT_PAGE_LOADED,time=20)
        self.element_is_clickable(ProductPageLocators.PRODUCT_PAGE_LOADED,time=20)
        for i in range (1, 10):
            second_pictures.click()
            try:
                self.element_is_visible(SELECTED_SECOND_PICTURES,time=1)
                break
            except TimeoutException:
                print("Second pictures is not clickable")

        self.element_is_visible(SELECTED_SECOND_PICTURES,time=20)
        self.driver.save_screenshot('screen_product_page.png')
