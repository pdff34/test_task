from PageObject.BaseApp import BasePage
from selenium.webdriver.common.by import By
import random

class SearchPageLocators:

    LIST_OF_PAGING = (By.XPATH, "//div[text() = 'Дальше']")
    LIST_OF_PRODUCT = (By.XPATH, "(//div[contains(@class, 'widget-search-result-container')]//div[@class='ko1'])")

class SearchPage(BasePage):

    '''
    Скроллим до начала пейджинга страницы. Так как шапка будет закрывать часть страницы,
    после скролла поднимаемся немного вверх.
    '''
    def scroll_to_paging_and_screen(self):

        header_heigh = -500
        self.scroll_to_element(SearchPageLocators.LIST_OF_PAGING, header_heigh)
        self.driver.save_screenshot('screen_paging.png')

    '''
    Находим товары с двумя картинками, затем выбираем любой из найденных товаров перед 
    этим делая скролл до него.
    '''
    def search_product_with_two_pictures_and_select_random(self):
        product_list = self.find_elements(SearchPageLocators.LIST_OF_PRODUCT,time=20)
        product_count = len(product_list)
        print(f"Count products with two pictures: {product_count}")
        number_random_product = random.randint(1, product_count)
        random_locator = f"(//div[contains(@class, 'widget-search-result-container')]//div[@class='ko1']/ancestor::a)[{number_random_product}]"
        RANDOM_PRODUCT = (By.XPATH, random_locator)
        product_href = self.find_element(RANDOM_PRODUCT,time=20).get_attribute('href')
        self.swith_to_new_tab_by_href(product_href)
