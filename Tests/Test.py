from PageObject.OzonSearchPage import SearchPage
from PageObject.OzonMainPage import MainPage
from PageObject.OzonProductPage import ProductPage

def test_ozon(browser):
    ozon_test = MainPage(browser)
    ozon_test.go_to_site()
    ozon_test.pick_city_for_search_rtx()
    ozon_test.enter_word("rtx 3090")
    ozon_test.click_on_the_search_button()
    ozon_test = SearchPage(browser)
    ozon_test.scroll_to_paging_and_screen()
    ozon_test.search_product_with_two_pictures_and_select_random()
    ozon_test = ProductPage(browser)
    ozon_test.select_second_pictures()
    ozon_test = MainPage(browser)
    ozon_test.go_to_catalog_page()
    ozon_test.select_catalog_and_check_home_and_garden()

