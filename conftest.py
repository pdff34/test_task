import pytest
import undetected_chromedriver as uc


@pytest.fixture(scope="session")
def browser():
    driver = uc.Chrome()
    driver.maximize_window()
    yield driver
    driver.quit()